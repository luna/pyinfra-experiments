import os
from tasks.deploy_hubs import deploy_hubs
from pyinfra.operations import apt

apt.update(cache_time=3600)
deploy_hubs(
    db_password=os.environ["HUBS_DB_PASSWORD"],
    client_domain=os.environ["HUBS_DOMAIN_CLIENT"],
    api_domain=os.environ["HUBS_DOMAIN_API"],
    proxy_domain=os.environ["HUBS_DOMAIN_PROXY"],
    assets_domain=os.environ["HUBS_DOMAIN_ASSETS"],
    link_domain=os.environ["HUBS_DOMAIN_LINK"],
    tls_done=os.environ.get("HUBS_TLS_DONE"),
    postgres_superuser_password=os.environ["HUBS_POSTGRES_SUPERUSER_PASSWORD"],
    phoenix_secret=os.environ.get("HUBS_PHOENIX_SECRET"),
    auth_public_key_path=os.environ["HUBS_AUTH_PUBLIC_KEY"],
    auth_private_key_path=os.environ["HUBS_AUTH_PRIVATE_KEY"],
    smtp_from_addr=os.environ["HUBS_SMTP_FROM_ADDR"],
    smtp_server=os.environ["HUBS_SMTP_SERVER"],
    smtp_hostname=os.environ["HUBS_SMTP_HOSTNAME"],
    smtp_port=os.environ["HUBS_SMTP_PORT"],
    smtp_username=os.environ["HUBS_SMTP_USERNAME"],
    smtp_password=os.environ["HUBS_SMTP_PASSWORD"],
    webpush_email=os.environ.get("HUBS_WEBPUSH_EMAIL"),
    webpush_private_key=os.environ.get("HUBS_WEBPUSH_PRIVATE_KEY"),
    webpush_public_key=os.environ.get("HUBS_WEBPUSH_PUBLIC_KEY"),
    guardian_secret=os.environ.get("HUBS_GUARDIAN_SECRET"),
)
