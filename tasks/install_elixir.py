from typing import Optional
from pyinfra.api import deploy
from pyinfra.operations import apt, files, server


@deploy("Install Elixir")
def install_elixir(
    version: Optional[str] = None,
    otp_version: Optional[str] = None,
    state=None,
    host=None,
):
    # elixir is non trivial to install because we can't
    # rely on the ubuntu package repo to be updated
    #
    # so we use the Erlang Solutions repository as recommended by elixir themselves.

    erlang_repo_deb_path = "/tmp/erlang-solutions.deb"
    files.download(
        name="download erlang solutions repo deb file",
        src="https://packages.erlang-solutions.com/erlang-solutions_2.0_all.deb",
        dest=erlang_repo_deb_path,
        state=state,
        host=host,
    )

    apt.deb(
        name="install erlang solutions repo",
        src=erlang_repo_deb_path,
        state=state,
        host=host,
    )

    # TODO: we don't need to update if we already installed the deb
    apt.update(cache_time=3600, state=state, host=host)

    # its in two separate steps as recommended by readme. who am i to judge
    apt.packages(
        name="install erlang",
        packages=[
            f"erlang={otp_version}" if otp_version else f"erlang",
            f"erlang-manpages={otp_version}" if otp_version else f"erlang-manpages",
        ],
        state=state,
        host=host,
    )
    apt.packages(
        name="install elixir",
        packages=[f"elixir={version}" if version else "elixir"],
        state=state,
        host=host,
    )

    server.shell(
        name="test elixir exists", commands=["elixir -v"], state=state, host=host
    )
