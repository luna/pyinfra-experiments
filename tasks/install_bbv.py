import secrets
from pathlib import Path

from pyinfra.api import deploy
from pyinfra.operations import apt, files, server, postgresql, git

DEFAULT_HOME_PATH = "/opt/bbv"
DEFAULT_REPO_PATH = "/opt/bbv/src"
DEFAULT_USER = "bbv"

BBV_REPO_URL = "https://gitlab.com/bigbigvpn/bbv.git"


@deploy("Install bbv")
def install_bbv(
    *,
    branch: str = "trunk",
    home_path: str = DEFAULT_HOME_PATH,
    repo_path: str = DEFAULT_REPO_PATH,
    installation_user: str = DEFAULT_USER,
    state=None,
    host=None,
):
    # requires a psql12 isntance installed. run install_postgres.py

    # assumes python3 is good python

    deps = ["python3.9-dev", "libffi-dev", "python3.9-venv"]

    apt.packages(
        name="install pre deps",
        packages=deps,
        state=state,
        host=host,
    )

    server.group(name="create bbv group", group="bbv", state=state, host=host)

    server.user(
        name=f"add user: {installation_user!r}",
        user=installation_user,
        present=True,
        home=home_path,
        shell=None,
        group=installation_user,
        groups=None,
        ensure_home=True,
        comment="bbv user",
        state=state,
        host=host,
    )

    git.repo(
        name="clone bbv git repo",
        src=BBV_REPO_URL,
        dest=repo_path,
        branch=branch,
        user=installation_user,
        group=installation_user,
        state=state,
        host=host,
    )

    wanted_subdirs = ("assets",)
    for subdir in wanted_subdirs:
        full_subdir_path = Path(home_path) / subdir
        files.directory(
            name=f"ensure {full_subdir_path!s} exists",
            path=str(full_subdir_path),
            present=True,
            user=installation_user,
            group=installation_user,
            recursive=True,
            state=state,
            host=host,
        )

    # install poetry
    server.shell(
        name="install poetry as bbv user",
        commands="curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3.9 -",
        sudo=True,
        sudo_user="bbv",
        state=state,
        host=host,
    )

    server.shell(
        name="install bbv python depedencies",
        commands="/opt/bbv/.poetry/bin/poetry install",
        sudo=True,
        sudo_user="bbv",
        chdir=repo_path,
        state=state,
        host=host,
    )

    # TODO customize db name
    psql_password = secrets.token_urlsafe(20)
    postgresql.role(
        name=f"Create the bbv PostgreSQL role (password = {psql_password!r})",
        role="bbv",
        password=psql_password,
        superuser=True,
        login=True,
        su_user="postgres",
        state=state,
        host=host,
    )

    postgresql.database(
        name="create psql db",
        database="bbv",
        su_user="postgres",
        state=state,
        host=host,
    )

    # TODO for sysadmin running this
    # run schema.sql
    # configure config.toml
