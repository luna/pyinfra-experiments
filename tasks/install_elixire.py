import secrets
from pathlib import Path

from pyinfra import host
from pyinfra.api import deploy
from pyinfra.operations import apt, files, server, postgresql, git

from .install_postgresql import install_psql_12
from .install_redis import install_redis

DEFAULT_HOME_PATH = "/opt/elixire"
DEFAULT_REPO_PATH = "/opt/elixire/src"
DEFAULT_USER = "elixire"

ELIXIRE_REPO_URL = "https://gitlab.com/elixire/elixire.git"


@deploy("Install elixire v3")
def install_elixire(
    *,
    root_domain: str,
    add_clamav: bool = False,
    home_path: str = DEFAULT_HOME_PATH,
    repo_path: str = DEFAULT_REPO_PATH,
    installation_user: str = DEFAULT_USER,
    state=None,
    host=None,
):

    install_psql_12(state=state, host=host)
    install_redis(state=state, host=host)

    # assumes python3 is good python

    deps = ["python3-dev", "libmagic1", "libmagic-dev", "libffi-dev", "python3-venv"]
    if add_clamav:
        deps.append("clamav")

    apt.packages(
        name="install pre deps",
        packages=deps,
        state=state,
        host=host,
    )

    server.group(name="create elixire group", group="elixire", state=state, host=host)

    server.user(
        name=f"add user: {installation_user!r}",
        user=installation_user,
        present=True,
        home=home_path,
        shell=None,
        group=installation_user,
        groups=None,
        ensure_home=True,
        comment="elixire user",
        state=state,
        host=host,
    )

    # we use v3 in this household :triumph:
    git.repo(
        name="clone elixire git repo",
        src=ELIXIRE_REPO_URL,
        dest=repo_path,
        branch="v3",
        user=installation_user,
        group=installation_user,
        state=state,
        host=host,
    )

    wanted_subdirs = ("images", "dumps", "thumbnails")
    for subdir in wanted_subdirs:
        full_subdir_path = Path(home_path) / subdir
        files.directory(
            name=f"ensure {full_subdir_path!s} exists",
            path=str(full_subdir_path),
            present=True,
            user=installation_user,
            group=installation_user,
            recursive=True,
            state=state,
            host=host,
        )

    server.shell(
        name="create python3 virtual env",
        commands=f"python3 -m venv {home_path}/env",
        sudo_user=installation_user,
        state=state,
        host=host,
    )

    server.shell(
        name="possibly update/install pip and wheel in venv",
        commands=f"./env/bin/python3 -m pip install -U pip wheel",
        sudo_user="elixire",
        chdir=home_path,
        state=state,
        host=host,
    )

    server.shell(
        name="install elixire python depedencies into venv",
        commands=f"../env/bin/python3 -m pip install -U --editable . -c constraints.txt",
        sudo_user="elixire",
        chdir=repo_path,
        state=state,
        host=host,
    )

    # TODO customize db name
    psql_password = secrets.token_urlsafe(20)
    postgresql.role(
        name=f"Create the elixire PostgreSQL role (password = {psql_password!r})",
        role="elixire",
        password=psql_password,
        superuser=True,
        login=True,
        su_user="postgres",
        state=state,
        host=host,
    )

    postgresql.database(
        name="create psql db",
        database="elixire",
        su_user="postgres",
        state=state,
        host=host,
    )

    # run schema.sql, then update domains so it uses correct root domain!
    server.shell(
        name="seed db with schema.sql file",
        commands=f"psql -d elixire -f {repo_path}/schema.sql",
        su_user="postgres",
        state=state,
        host=host,
    )
