from typing import Optional
from pyinfra.api import deploy
from pyinfra.operations import apt, systemd, postgresql, server, systemd, files


@deploy("Install PostgREST")
def install_postgrest(
    postgres_database: str,
    postgres_schema: str,
    version: str = "9.0.0",
    postgres_service: Optional[str] = None,
    postgres_user: str = "postgres",
    postgres_password: Optional[str] = None,
    postgres_host: str = "localhost",
    postgres_port: str = "5432",
    state=None,
    host=None,
):
    download_url = f"https://github.com/PostgREST/postgrest/releases/download/v{version}/postgrest-v{version}-linux-static-x64.tar.xz"

    output_path = f"/tmp/postgrest-{version}.tar.xz"

    files.download(
        name=f"download binary package to {output_path}",
        src=download_url,
        dest=output_path,
        state=state,
        host=host,
    )

    server.shell(
        name="extract binary",
        commands=[f"tar xvf {output_path} -C /usr/bin"],
        state=state,
        host=host,
    )

    files.template(
        name="install service",
        src="templates/postgrest.service.j2",
        dest="/etc/systemd/system/postgrest.service",
        state=state,
        host=host,
        postgres_service=postgres_service,
        postgres_user=postgres_user,
        postgres_password=postgres_password,
        postgres_host=postgres_host,
        postgres_port=postgres_port,
        postgres_database=postgres_database,
        postgres_schema=postgres_schema,
    )

    systemd.daemon_reload(state=state, host=host)
    systemd.service(
        name="ensure systemd unit is up",
        service="postgrest.service",
        running=True,
        enabled=True,
        state=state,
        host=host,
    )
