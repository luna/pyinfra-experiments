from pyinfra.api import deploy
from pyinfra.operations import apt, systemd, postgresql


@deploy("Install PostgreSQL 12")
def install_psql_12(postgres_superuser_password=None, state=None, host=None):
    apt.packages(
        name="Install psql12 packages",
        packages=[
            "postgresql-12",
            # i sure hope when ubuntu upgrades to 13 this doesnt break
            "postgresql-contrib",
            "postgresql-client-12",
        ],
        state=state,
        host=host,
    )

    # assumes the package creates postgresql@12-main.service

    wanted_unit = "postgresql@12-main.service"
    systemd.service(
        name="ensure systemd unit is up",
        service=wanted_unit,
        running=True,
        enabled=True,
        state=state,
        host=host,
    )

    if postgres_superuser_password:
        postgresql.sql(
            f"ALTER USER postgres WITH PASSWORD '{postgres_superuser_password}'",
            su_user="postgres",
            state=state,
            host=host,
        )
