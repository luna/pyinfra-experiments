import os
import secrets
from pathlib import Path
from typing import Optional

from pyinfra.api import deploy
from pyinfra.operations import apt, files, server, postgresql, git, python
from pyinfra.facts import hardware

from tasks.install_elixir import install_elixir
from tasks.install_nodejs import install_nodejs
from tasks.install_croc import install_croc
from tasks.install_postgresql import install_psql_12
from tasks.install_postgrest import install_postgrest

# todo make repo paths and template paths properly parametrized inside
# the task function (since someone might want a diff homedir for the hubs user)
RETICULUM_PATH = "/opt/hubs/reticulum"
DIALOG_PATH = "/opt/hubs/dialog"
NGINX_HUBS_PATH = "/etc/nginx/sites-available/hubs.conf"
CLIENT_PATH = "/opt/hubs/client"

REPOS = {
    "https://github.com/mozilla/reticulum": RETICULUM_PATH,
    "https://github.com/mozilla/hubs": "/opt/hubs/client",
    "https://github.com/mozilla/dialog": DIALOG_PATH,
}

TEMPLATES = {
    "templates/hubs/prod.secret.exs.j2": f"{RETICULUM_PATH}/config/prod.secret.exs",
    "templates/hubs/reticulum.service": f"/etc/systemd/system/hubs-reticulum.service",
    "templates/hubs/client.service": f"/etc/systemd/system/hubs-client.service",
    "templates/hubs/admin.service": f"/etc/systemd/system/hubs-admin.service",
    "templates/hubs/dialog.service": f"/etc/systemd/system/hubs-dialog.service",
    "templates/hubs/dialog_auth_key.pem": f"{DIALOG_PATH}/authkey.pem",
    "templates/hubs/nginx.conf": NGINX_HUBS_PATH,
    "templates/hubs/client.env": f"{CLIENT_PATH}/.env",
    "templates/hubs/admin.env": f"{CLIENT_PATH}/admin/.env",
}

DEFAULT_HOME_PATH = "/opt/hubs"
DEFAULT_USER = "hubs"

BBV_REPO_URL = "https://gitlab.com/bigbigvpn/bbv.git"


@deploy("Install hubs")
def deploy_hubs(
    *,
    install_user_home_path: str = DEFAULT_HOME_PATH,
    install_user: str = DEFAULT_USER,
    db_user: str = "hubs",
    db_name: str = "hubs",
    db_password: str,
    client_domain: str,
    api_domain: str,
    proxy_domain: str,
    assets_domain: str,
    link_domain: str,
    postgres_superuser_password: str,
    phoenix_secret: Optional[str] = None,
    tls_done: Optional[str] = None,
    auth_public_key_path: str,
    auth_private_key_path: str,
    smtp_from_addr: str,
    smtp_server: str,
    smtp_hostname: str,
    smtp_port: str,
    smtp_username: str,
    smtp_password: str,
    webpush_email: Optional[str] = None,
    webpush_private_key: Optional[str] = None,
    webpush_public_key: Optional[str] = None,
    guardian_secret: Optional[str] = None,
    state=None,
    host=None,
):

    with open(auth_public_key_path, "r") as fd:
        auth_public_key = fd.read()
    with open(auth_private_key_path, "r") as fd:
        auth_private_key = fd.read()

    # good tool to use, regardless
    install_croc(state=state, host=host)

    # those are the actual infra deps
    install_nodejs(node_repo_name="node_14.x", state=state, host=host)
    install_psql_12(
        postgres_superuser_password=postgres_superuser_password, state=state, host=host
    )
    install_postgrest(
        postgres_password=postgres_superuser_password,
        postgres_service="postgresql@12-main.service",
        postgres_database="hubs",
        postgres_schema="ret0_admin",
        state=state,
        host=host,
    )

    def need_tls_config(state, host):
        if tls_done != "1":
            # ssh in
            # 'certbot certonly' with standalone mode
            #
            # select all the domains we just created
            raise Exception(
                "Certbot is now installed. Run it in 'certbot certonly' mode, then set HUBS_TLS_DONE to 1"
            )

    # TODO maybe firewall all ports except ssh, https, 4443 (for dialog),
    # and mosh ports???
    apt.packages(packages=["certbot", "nginx"], state=state, host=host)

    python.call(
        name="check tls config", function=need_tls_config, state=state, host=host
    )

    download_and_install = {
        "https://packages.erlang-solutions.com/erlang/debian/pool/esl-erlang_21.3.8.17-1~ubuntu~bionic_amd64.deb": "/tmp/esl-erlang-21.3.8.17.deb",
        "https://packages.erlang-solutions.com/erlang/debian/pool/elixir_1.8.2-1~ubuntu~bionic_amd64.deb": "/tmp/elixir-1.8.2.deb",
    }

    for source_url, output_path in download_and_install.items():
        files.download(
            name=f"download {output_path} deb file",
            src=source_url,
            dest=output_path,
            state=state,
            host=host,
        )

        apt.deb(
            name=f"install {output_path}",
            src=output_path,
            state=state,
            host=host,
        )

    server.group(
        name=f"create {install_user!r} group",
        group=install_user,
        state=state,
        host=host,
    )

    server.user(
        name=f"add user: {install_user!r}",
        user=install_user,
        present=True,
        home=install_user_home_path,
        shell="/usr/bin/bash",
        group=install_user,
        groups=None,
        ensure_home=True,
        comment="hubs user",
        state=state,
        host=host,
    )

    for repo_url, repo_path in REPOS.items():
        git.repo(
            name=f"clone {repo_url} git repo to {repo_path}",
            src=repo_url,
            dest=repo_path,
            user=install_user,
            group=install_user,
            state=state,
            host=host,
        )

    postgresql.role(
        name=f"Create the hubs PostgreSQL user",
        role=db_user,
        password=db_password,
        superuser=True,
        login=True,
        su_user="postgres",
        state=state,
        host=host,
    )

    postgresql.database(
        name=f"create psql db: {db_name}",
        database=db_name,
        su_user="postgres",
        state=state,
        host=host,
    )

    # TODO echo import_config 'prod.secrets.exs' to end of prod.exs file
    # so that everything we made is overwritten by whatever mozilla wants
    #
    # (maybe we also should spit this as a hubs' pr, or a fork of hubs)
    #
    # TODO apply patch to remove Sentry usage across shit (either patch or fork)

    server.shell(
        name="install reticulum dependencies",
        commands=[
            "cd /opt/hubs/reticulum && env MIX_ENV=prod mix local.hex --force",
            "cd /opt/hubs/reticulum && env MIX_ENV=prod mix local.rebar --force",
            "cd /opt/hubs/reticulum && env MIX_ENV=prod mix deps.get --only prod",
            "cd /opt/hubs/reticulum && env MIX_ENV=prod mix compile",
        ],
        su_user=install_user,
        state=state,
        host=host,
    )

    def check_reticulum_configs(state, host):
        # ssh in
        # su -l hubs
        # cd /opt/hubs/reticulum
        # $ env MIX_ENV=prod mix phx.gen.secret
        # put result in HUBS_PHOENIX_SECRET env var
        # $ env MIX_ENV=prod mix guardian.gen.secret
        # put result in HUBS_GUARDIAN_SECRET env var
        #
        # now get an elixir shell
        # $ iex
        # > {private_key, public_key} = :crypto.generate_key(:ecdh, :prime256v1)
        # > Base.url_encode64(private_key, padding: false)
        # put this result in HUBS_WEBPUSH_PRIVATE_KEY env var
        # > Base.url_encode64(public_key, padding: false)
        # put this result in HUBS_WEBPUSH_PUBLIC_KEY env var
        # put an email on HUBS_SUBJECT_EMAIL (e.g webmistress@your.domain)
        if not (
            phoenix_secret
            and guardian_secret
            and webpush_email
            and webpush_private_key
            and webpush_public_key
        ):
            raise Exception(
                "Now that the reticulum deps are installed, configure MANY secrets :("
            )

    python.call(
        name="check reticulum config",
        function=check_reticulum_configs,
        state=state,
        host=host,
    )

    # generate tls paths based on domains set for each component
    # forces us to be on certbot, but oh well.
    components = ("client", "api", "proxy", "assets", "link")

    tls_paths = {}
    domains = {}

    chain_path = f"/etc/letsencrypt/live/{client_domain}/chain.pem"
    fullchain_path = f"/etc/letsencrypt/live/{client_domain}/fullchain.pem"
    key_path = f"/etc/letsencrypt/live/{client_domain}/privkey.pem"
    for scope in components:
        tls_paths[f"{scope}_tls_chain_path"] = chain_path
        tls_paths[f"{scope}_tls_fullchain_path"] = fullchain_path
        tls_paths[f"{scope}_tls_key_path"] = key_path
        domains[scope] = locals()[f"{scope}_domain"]

    for template_file, target_file in TEMPLATES.items():
        files.template(
            name=f"config file ({target_file})",
            src=template_file,
            dest=target_file,
            env_dict={
                **tls_paths,
                **{
                    "db_name": db_name,
                    "db_user": db_user,
                    "db_password": db_password,
                    "phoenix_secret": phoenix_secret,
                    "client_domain": client_domain,
                    "api_domain": api_domain,
                    "proxy_domain": proxy_domain,
                    "assets_domain": assets_domain,
                    "link_domain": link_domain,
                    "postgres_superuser_password": postgres_superuser_password,
                    "install_user": install_user,
                    "install_user_home_path": install_user_home_path,
                    # TODO configurable interface
                    "host_ip": host.get_fact(hardware.Ipv4Addrs)["eth0"][0],
                    "dialog_path": f"{install_user_home_path}/dialog",
                    "client_path": f"{install_user_home_path}/client",
                    "reticulum_path": f"{install_user_home_path}/reticulum",
                    "auth_public_key": auth_public_key,
                    "auth_private_key": auth_private_key,
                    "smtp_from_addr": smtp_from_addr,
                    "smtp_server": smtp_server,
                    "smtp_hostname": smtp_hostname,
                    "smtp_port": smtp_port,
                    "smtp_username": smtp_username,
                    "smtp_password": smtp_password,
                    "webpush_email": webpush_email,
                    "webpush_private_key": webpush_private_key,
                    "webpush_public_key": webpush_public_key,
                    "guardian_secret": guardian_secret,
                    "domains": domains,
                },
            },
            state=state,
            host=host,
        )

    server.shell(
        name="setup reticulum database",
        commands=[
            "cd /opt/hubs/reticulum && env MIX_ENV=prod mix ecto.create",
            "cd /opt/hubs/reticulum && mkdir -p /opt/hubs/reticulum/storage/{prod,assets,docs}",
        ],
        su_user=install_user,
        state=state,
        host=host,
    )

    # configure permissions for all of the tls paths we want
    commands = [
        "chmod 705 /etc/letsencrypt/live",
        "chmod 705 /etc/letsencrypt/archive",
    ]
    for path in (chain_path, fullchain_path, key_path):
        commands.append(
            f"setfacl -m u:{install_user}:r {path}",
        )

    server.shell(
        commands,
        name="configure tls cert permissions",
        state=state,
        host=host,
    )

    files.link(
        name="turn on nginx website",
        path=f"/etc/nginx/sites-enabled/hubs.conf",
        target=NGINX_HUBS_PATH,
        state=state,
        host=host,
    )

    server.shell(
        name="reload service files",
        commands=[
            "systemctl daemon-reload",
            "systemctl enable nginx.service",
        ],
        state=state,
        host=host,
    )

    # get your user as admin
    #  Ret.Account |> Ret.Repo.all() |> Enum.at(0) |> Ecto.Changeset.change(is_admin: true) |> Ret.Repo.update!()

    # i fucking hate npm
    # server.shell(
    #    name="install client dependencies",
    #    commands=[
    #        "cd /opt/hubs/client && npm ci",
    #        "cd /opt/hubs/dialog && npm ci",
    #        "cd /opt/hubs/client/admin && npm ci",
    #    ],
    #    su_user=install_user,
    #    state=state,
    #    host=host,
    # )
    #
    #
    # also run npm run build on client and client admin
    #
    # install https://hubs.mozilla.com/docs/hubs-cloud-asset-packs.html thru admin panel
