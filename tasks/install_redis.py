from pyinfra.api import deploy
from pyinfra.operations import apt, files, server, git


@deploy("Install Redis")
def install_redis(state=None, host=None):
    apt.ppa(
        name="add redis ppa",
        src="ppa:redislabs/redis",
        present=True,
        state=state,
        host=host,
    )
    apt.update(cache_time=3600, state=state, host=host)
    apt.packages(
        name="install redis package",
        packages=[
            "redis",
        ],
        state=state,
        host=host,
    )
