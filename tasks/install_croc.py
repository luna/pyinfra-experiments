from pyinfra import host
from pyinfra.api import deploy
from pyinfra.operations import apt, files, server


@deploy("Install croc")
def install_croc(
    *,
    version: str = "9.5.1",
    state=None,
    host=None,
):
    # TODO support 32bit linux, other os'es, .tar.gz method, etc. if needed
    deb_url = f"https://github.com/schollz/croc/releases/download/v{version}/croc_{version}_Linux-64bit.deb"

    deb_path = "/tmp/croc.deb"
    files.download(
        name="download croc deb file",
        src=deb_url,
        dest=deb_path,
        state=state,
        host=host,
    )

    apt.deb(
        name="install croc deb",
        src=deb_path,
        state=state,
        host=host,
    )

    server.shell(
        name="test croc exists",
        commands=["croc --version"],
        state=state,
        host=host,
    )
