from pyinfra.api import deploy
from pyinfra.operations import apt, files, server


@deploy("Install UFW and setup allowed HTTP, SSH and MOSH")
def setup_ufw(state=None, host=None):
    apt.packages(name="install ufw", packages=["ufw"], state=state, host=host)
    server.shell(
        name="ufw: allow http, https, ssh, mosh",
        commands=[
            "ufw allow http",
            "ufw allow https",
            "ufw allow ssh",
            "ufw allow 60001:60999/udp",
        ],
        state=state,
        host=host,
    )
