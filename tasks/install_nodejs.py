from pyinfra import host
from pyinfra.api import deploy
from pyinfra.operations import apt, files, server


@deploy("Install NodeJS")
def install_nodejs(
    *,
    channel: str = "stable",
    node_repo_name: str = "node_16.x",
    add_all_deps: bool = True,
    state=None,
    host=None,
):
    """Install NodeJS via NodeSource.

    This process has been codified from
    https://github.com/nodesource/distributions/blob/master/README.md#deb
    """

    # TODO: ensure architecture is in supported list

    apt.packages(
        name="install deps",
        packages=["apt-transport-https", "lsb-release", "curl", "gnupg"],
        state=state,
        host=host,
    )

    apt.key(
        name="add nodesource repo key",
        src="https://deb.nodesource.com/gpgkey/nodesource.gpg.key",
        state=state,
        host=host,
    )

    distro = host.fact.lsb_release["codename"]
    repo_entry = f"deb https://deb.nodesource.com/{node_repo_name} {distro} main"
    repo_src_entry = (
        f"deb-src https://deb.nodesource.com/{node_repo_name} {distro} main"
    )

    repo_path = "/etc/apt/sources.list.d/nodesource.list"
    files.file(name="ensure repo file exists", path=repo_path, state=state, host=host)

    for entry in (repo_entry, repo_src_entry):
        files.line(
            name=f"ensure repositories are declared ({entry})",
            path=repo_path,
            line=entry,
            state=state,
            host=host,
        )

    apt.update(cache_time=3600, state=state, host=host)

    apt.packages(
        name="install nodejs (and useful deps)",
        packages=["nodejs", "gcc", "g++", "make"],
        state=state,
        host=host,
    )

    server.shell(
        name="test nodejs exists", commands=["node -v"], state=state, host=host
    )
