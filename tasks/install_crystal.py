from pyinfra.api import deploy
from pyinfra.operations import apt, files, server


@deploy("Install Crystal")
def install_crystal(
    *, channel: str = "stable", add_all_deps: bool = True, state=None, host=None
):
    """Install Crystal.

    This process has been codified from
    https://crystal-lang.org/install/on_ubuntu/
    """
    crystal_repo_entry_path = "/etc/apt/sources.list.d/crystal.list"
    repo_entry = f"deb https://dl.bintray.com/crystal/deb all {channel}"

    files.file(path=crystal_repo_entry_path, state=state, host=host)
    files.line(
        name="ensure repo line is declared",
        path=crystal_repo_entry_path,
        line=repo_entry,
        state=state,
        host=host,
    )

    apt.key(
        name="add crystal repo keys",
        keyserver="hkp://keyserver.ubuntu.com:80",
        keyid="379CE192D401AB61",
        state=state,
        host=host,
    )
    apt.update(cache_time=3600, state=state, host=host)

    apt.packages(
        name="install crystal and libssl-dev",
        packages=[
            "crystal",
            "libssl-dev",
        ],
        state=state,
        host=host,
    )

    if add_all_deps:
        apt.packages(
            name="install extra packages for full crystal feature",
            packages=[
                "libxml2-dev",
                "libyaml-dev",
                "libgmp-dev",
                "libz-dev",
            ],
            state=state,
            host=host,
        )

    server.shell(
        name="test crystal exists", commands=["crystal -v"], state=state, host=host
    )
