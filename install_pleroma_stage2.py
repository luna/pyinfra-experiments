# install_pleroma_stage2: after 'mix pleroma.instance gen'

from pyinfra.operations import server, postgresql
from pathlib import Path

PLEROMA_PATH = "/opt/pleroma"
PLEROMA_USER = "pleroma"

server.shell(
    name="move config to prod.secret.exs",
    commands=f"cd {PLEROMA_PATH} && sudo -Hu pleroma cp config/generated_config.exs config/prod.secret.exs",
)

server.shell(
    name="setup psql user and db",
    commands=f"sudo -u postgres psql -f {PLEROMA_PATH}/config/setup_db.psql",
)


server.shell(
    name="run database migrations",
    commands=f"cd {PLEROMA_PATH} && sudo -Hu pleroma env MIX_ENV=prod mix ecto.migrate",
)
