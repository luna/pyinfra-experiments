import urllib.parse
import sys
import json

parsed = urllib.parse.urlparse(sys.argv[1])
query = urllib.parse.parse_qs(parsed.query)

print(
    "1st message to send:",
    json.dumps(
        {
            "topic": query["auth_topic"][0],
            "event": "phx_join",
            "payload": {},
            "ref": "0",
        }
    ),
)
print(
    "1st message to send:",
    json.dumps(
        {
            "topic": query["auth_topic"][0],
            "event": "auth_verified",
            "payload": {
                "token": query["auth_token"][0],
                "payload": query["auth_payload"][0],
            },
            "ref": "2",
        }
    ),
)
