# install_pleroma_stage1.py - install pleroma stage 1
#
# this script installs pleroma for you!
#
# this does not install nginx.
#
# this is stage1! finishing it requires running 'mix pleroma.instance gen'
# as the pleroma user in the /opt/pleroma directory.
#
# then run stage2.

from pyinfra.operations import apt, server, git, files
from tasks.install_postgresql import install_psql_12
from tasks.install_elixir import install_elixir

PLEROMA_GIT_URL = "https://git.pleroma.social/pleroma/pleroma.git"
PLEROMA_PATH = "/opt/pleroma"
PLEROMA_USER = "pleroma"

install_psql_12()
install_elixir()

apt.packages(
    name="install wanted pleroma system depedencies",
    packages=[
        "sudo",
        "git",
        "build-essential",
        "cmake",
        "libmagic-dev",
        "ImageMagick",
        "ffmpeg",
        "exiftool",
        "libimage-exiftool-perl",
        "erlang-dev",
        "erlang-parsetools",
    ],
)

server.group(name="create pleroma group", group="pleroma")

server.user(
    name=f"add pleroma user: {PLEROMA_USER!r}",
    user=PLEROMA_USER,
    present=True,
    home="/var/lib/pleroma",
    shell=None,
    group="pleroma",
    groups=None,
    ensure_home=True,
    comment="Pleroma user",
)

files.directory(
    name=f"ensure {PLEROMA_PATH} exists",
    path=PLEROMA_PATH,
    present=True,
    user=PLEROMA_USER,
    group="pleroma",
    recursive=True,
)

# we clone develop in this household :triumph:
git.repo(
    name="clone pleroma repo",
    src=PLEROMA_GIT_URL,
    dest=PLEROMA_PATH,
    branch="develop",
    user=PLEROMA_USER,
    group="pleroma",
)

# download pleroma deps via mix

server.shell(
    name="download pleroma deps",
    chdir=PLEROMA_PATH,
    sudo_user=PLEROMA_USER,
    commands=[
        "mix local.hex --force",
        "mix local.rebar --force",
        "mix deps.get",
    ],
)


# compile deps and compile pleroma
server.shell(
    name="compile pleroma deps",
    commands=f"cd {PLEROMA_PATH} && sudo -Hu pleroma mix deps.compile",
)
server.shell(
    name="compile pleroma itself",
    commands=f"cd {PLEROMA_PATH} && sudo -Hu pleroma mix compile",
)

# stage1 is complete. run the following:
# mix pleroma.instance gen
